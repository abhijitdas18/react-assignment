import  React from 'react';

import Flute from '../images/Flute.jfif';
import Guitar from '../images/Guitar.jfif';
import Harmonium from '../images/Harmonium.jfif';
import Piano from '../images/Piano.jfif';
import Veena from '../images/Veena.jfif';
import Tabla from '../images/Tabla-Sitar.jfif';


class DisplayImage extends React.Component {

    constructor(props){
        super(props);

        this.onClickNext = this.onClickNext.bind(this);    
        this.onClickPrevious = this.onClickPrevious.bind(this);  

        this.state = {
            index : 0,
            imgList : [Flute, Guitar, Harmonium, Piano, Veena, Tabla]
        }
    }

    onClickNext(){
        console.log("next");
        if(this.state.index + 1  === this.state.imgList.length) {
            this.setState({ 
                index : 0
            })
        } else{
            this.setState({
                index : this.state.index + 1
            })
        }
    }

     onClickPrevious(){
        if(this.state.index -1 === -1) {
            this.setState({
                 index : this.state.imgList.length - 1
            })
        } else{
            this.setState({
                index : this.state.index - 1
            })
        }
    } 

    render() {
        return (
            <div>
                <br />
                <button  style = {{color: 'blue'}}  onClick = {this.onClickNext}>Next</button>                 
                <img style = {{width: 120, height: 120}}  src = {this.state.imgList[this.state.index]} alt = "temp2"/> 
                <button style = {{color: 'blue'}} onClick = {this.onClickPrevious}>Previous</button>   
                <br />
                <div style = {{display: 'flex',  paddingLeft:'100px', paddingTop:'20px'}}>
                    {this.state.index}                  
                </div>
            </div>
        )
    }

}
export default DisplayImage;