import logo from './images/Flute.jfif';
import './App.css';
import DisplayImage from './components/DisplayImage'
import Header from './components/Header';

function App() {
  return (
    <div>
      <Header />
      <DisplayImage />
    </div>   
  );
}

export default App;
