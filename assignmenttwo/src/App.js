import Header from './components/inmemoryjson/Header';
import DisplayAndAddSongs from './components/inmemoryjson/DisplayAndAddSongs';
import AllSongsJSON from './components/inmemoryjson/AllSongsJSON';


function App() {
  return (
    <div>
      <Header /> 
      <AllSongsJSON />    
    </div>   
  );
}
export default App;
