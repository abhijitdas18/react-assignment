import React from 'react';

class DisplayAndAddSongs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            movie : '',
            title : '',
            length : '',
            singer : '',
            lists: []
        }
        console.log("constructor");
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange =(event) =>  {
        let nam = event.target.name;
        let val = event.target.value;   
        this.setState({[nam]: val});
        console.log("state handleChange for movie :" + this.state.movie);        
    }

    componentDidMount() {
        this.setState({
            lists: this.props.songs
        })
        console.log("componentDidMount");
    }

    handleSubmit = (event) => {

        event.preventDefault();

    }

    render() {

        console.log("render");

        return (
            <div>               

                <h4 style={{ color: 'blue' }}> Songs List </h4>
                <table border="1" style={{ color: 'blue' }}>
                    <tr>
                        <th>Movie</th>
                        <th>Title</th>
                        <th>Song Length</th>
                        <th>Singer</th>
                    </tr>
                    {this.state.lists.map((p) => (

                        <tr key={p.id}>
                            <td>{p.movie}</td>
                            <td>{p.title}</td>
                            <td>{p.length}</td>
                            <td>{p.singer}</td>
                        </tr>

                    )
                    )}
                </table>

                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label style={{ color: 'blue' }}>
                            <input type="text" value={this.state.movie} onChange={this.handleChange} name="movie" /> Movie Name
                        </label>
                    </div>
                    <div>
                        <label style={{ color: 'blue' }}>
                            <input type="text" value={this.state.title} onChange={this.handleChange} name="title" /> Song Title
                        </label>
                    </div>
                    <div>
                        <label style={{ color: 'blue' }}>
                            <input type="text" value={this.state.length} onChange={this.handleChange} name="length" /> Song Length
                        </label>
                    </div>
                    <div>
                        <label style={{ color: 'blue' }}>
                            <input type="text" value={this.state.singer} onChange={this.handleChange} name="singer" /> Singer
                        </label>
                    </div>

                    <input style={{ color: 'blue' }} type="submit" value="Add Song" />
                </form>
            </div>
        )
    }
}

export default DisplayAndAddSongs;