import React  from 'react';
import DisplayAndAddSongs from './DisplayAndAddSongs';


class AllSongsJSON extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name : 'abhijit',
            songscollection: [
              
                {
                    movie: 'Anurodh',
                    title: 'JabDardNahiTha',
                    length: '4.50',
                    singer: 'Kishore'
                },
                {
                    movie: 'KabhiKabhi',
                    title: 'MePalDoPal',
                    length: '3.45',
                    singer: 'Mukesh'
                },
                {
                    movie: 'Aradhana',
                    title: 'AajaTujhko',
                    length: '4.45',
                    singer: 'Rafi'
                },
                {
                    movie: 'MungaruMale',
                    title: 'Anisuthidhe',
                    length: '5.54',
                    singer: 'SanuNigam'
                },
                {
                    movie: 'Aashique',
                    title: 'EkSanamChahiya',
                    length: '3.25',
                    singer: 'KumarSanu'
                }
            ]
        }
    }
    render() {
        var count = Object.keys(this.state.songscollection).length;
        return(
            <div>
                <h3>Display Song Info using in memory JSON</h3>              
                <p>Number of songs in the playlist : {count} </p>
                <hr />
                <DisplayAndAddSongs songs = {this.state.songscollection}/>
                {/* <DisplayAndAddSongs songs = {this.state.name}/> */}
            </div>
        );
    }

}

export default AllSongsJSON;