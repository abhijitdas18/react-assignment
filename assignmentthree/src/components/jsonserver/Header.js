import React from 'react'

const Header = () => {
    return (
        <header>
           <h3 style={{color : 'red'}}> FED React : Assignment3 : Router, JSON Server </h3>
           <hr/>
           <h4 style={{color : 'blue'}}>Display Song info from the JSON Server  </h4>
           <hr/>
        </header>
    )
}
export default Header;