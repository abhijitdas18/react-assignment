import React from 'react'
 

class  GetOnlineSongs extends React.Component {
    constructor(props){
        super(props);

        this.state = {   
            posts : []
        };
    }

    componentDidMount() {
        fetch("http://localhost:3000/songscollection")
        .then(res => res.json())
        .then((data) => {
                this.setState({
                    posts : data
                })
                console.log("Data received : " + this.state.posts); 
            })  
            .catch("Error ::::" + console.log)    
    }

    render() {
       
            return(
                <div>
                    <h4 style={{color : 'blue'}}> Songs List </h4>
                    <table border = "1" style={{color : 'blue'}}>
                        <tr>
                            <th>Movie</th>
                            <th>Title</th>
                            <th>Song Length</th>
                            <th>Singer</th>
                        </tr>
                        {this.state.posts.map((p) => (  
                           
                        <tr key= {p.id}>
                            <td>{p.movie}</td>
                            <td>{p.title}</td>
                            <td>{p.length}</td>
                            <td>{p.singer}</td>
                        </tr>
                    
                        )
                        )}
                    </table>
                </div>
            );
        }
}
export default GetOnlineSongs;