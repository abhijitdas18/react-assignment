import React from 'react'

import Header from "./Header";
import About from "./About";
import GetOnlineSongs from "./GetOnlineSongs";
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Sample from './Sample';

class JsonServerImplementation extends React.Component {
 render(){


    return(
        <div>
            <Header />
            <Router>
                <Link to = "">About </Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Link to = "/songs"> Songs</Link>

                <Route exact path="/" component={About} />
                <Route  path="/songs" component={GetOnlineSongs} />

             {/* <Header />
             <About />             
             <GetOnlineSongs />
             <Sample />    */}
             </Router>       
        </div>
    );
 }   
}

export default JsonServerImplementation;