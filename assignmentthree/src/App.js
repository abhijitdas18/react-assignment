import './App.css';
import JsonServerImplementation from './components/jsonserver/JsonServerImplementation';

function App() {
  return (
    <div>  
      <JsonServerImplementation />
    </div>   
  );
}
export default App;
