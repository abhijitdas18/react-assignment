import './App.css';
import FormikImplementation from './components/FormikImplementation';

function App() {
  return (
  
    <div>
      <div>
        <div>
         
          <FormikImplementation />
    
        </div>      
      </div>
    </div>
  );
}

export default App;
