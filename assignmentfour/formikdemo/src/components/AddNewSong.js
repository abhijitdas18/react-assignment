import React from 'react';
import { withFormik, Form, Field } from 'formik';
import * as Yup from 'yup';
import uuid from 'uuidv4';
import axios from 'axios';
//import './style.css';

const AddNewForm = ({values, errors, touched, isSubmitting }) => (
    <div>
            <br/>
            <table border = "1" style={{color : 'blue', width:'30%', textAlign:'left'}}>
                <tr style={{border: 1}}>
                   <td style={{border:1}}>
                   <h3 style={{color : 'blue'}}> Songs List </h3>
                        <Form style={{color : 'blue'}}>
                            <div>
                                <label>
                                    Movie
                                </label>
                            </div>
                            <div>
                                <Field type = "text" name="movie" placeholder = "Enter movie name" />
                                {touched.movie && errors.movie && <span style ={{color :'red'}}> {errors.movie} </span>}
                            </div>

                            <div>
                                <label>
                                Song Title
                                </label>
                            </div>
                            <div>
                                <Field type = "text" name="title" placeholder = "Enter song title" />
                                {touched.title && errors.title && <span style ={{color :'red'}}> {errors.title} </span>}
                            </div>

                            <div>
                                <label>
                                    Song Length
                                </label>
                            </div>
                            <div>
                                <Field type = "text" name="length" placeholder = "Enter song length" />
                                {touched.length && errors.length && <span style ={{color :'red'}}> {errors.length} </span>}
                            </div>

                            <div>
                                <label>
                                    Singer
                                </label>
                            </div>
                            <div>
                                <Field type = "text" name="singer" placeholder = "Enter singer name" />
                                {touched.singer && errors.singer && <span style ={{color :'red'}}> {errors.singer} </span>}
                            </div>
                            
                            <br/>
                            <button type="submit" className="btn btn-info" disabled={isSubmitting} >Submit</button>
                          
                        </Form>
                   </td>
                </tr>
                <br/>
            </table>
    </div>
)

const FormikSongForm = withFormik({
    mapPropsToValues({movie, title, length, singer}) {
       // alert("this is mapPropsToValues ");
        return {
            movie : movie || '',
            title : title || '',
            length : length || '',
            singer : singer || ''
        }
    }, //1

    validationSchema : Yup.object().shape({    
        movie: Yup.string().min(3, 'Movie must be 3 characters in length').required('Movie is required'),
        title: Yup.string().min(3, 'Title must be 3 characters in length').required('Title is required'),
        length: Yup.string().min(3, 'Song must be 3 characters in length').required('Song Length is required'),
        singer: Yup.string().min(3, 'Singer must be 3 characters in length').required('Singer is required')
    }), //2

    handleSubmit(values, {resetForm, setSubmitting, setErrors}) {
        console.log("Values :::: " + values);
        console.log("Movie : "+ values.movie);
        console.log("Song :" + values.singer);

        const song = {         
           movie: values.movie,
           title: values.title,
           length : values.length,
           singer : values.singer         
        };       

        const res = axios.post('http://localhost:3000/songscollection', song);
       
        setTimeout(() => {
            if(values.movie === 'abc') {
                setErrors({movie:'You can not add any name in movie'})
            } else{
                resetForm();
                //alert(JSON.stringify(values));
            }
            setSubmitting(false);
        },  2000);
    } // 3
})(AddNewForm)

export default FormikSongForm;

