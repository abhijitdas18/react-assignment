import React from 'react';

class AddNewFromButton extends React.Component {

    constructor(props){
        super(props);  
        this.state = { 
            viewForm: false
        };   

        this.onAddFormClicked = this.onAddFormClicked.bind(this);
    }

    onAddFormClicked = (event) => {

        event.preventDefault();
        console.log("Add is clicked");
        this.setState({
             viewForm: true 
        });      
       
    }

    render() {
       
        return(
            <div>
                <form onSubmit={this.onAddFormClicked}>  
                            <input style={{ color: 'blue' }} type="submit" value="Add Song" />
                </form>
            </div>
        )
       
    }
}

export default AddNewFromButton;

