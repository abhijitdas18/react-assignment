import React from 'react'
import {Link} from 'react-router-dom';

class  GetSongsFromJSON extends React.Component {
    constructor(props){
        super(props);

        this.state = {        
            posts : []
        };
    }    

    componentDidMount() {
        fetch("http://localhost:3000/songscollection")
        .then(res => res.json())
        .then((data) => {
                this.setState({
                    posts : data
                })
                console.log("Data received : " + this.state.posts); 
            })  
            .catch("Error ::::" + console.log)    
    }
  
    render() {
       
            return(
                <div>  
                    <br/>
                    <h3 style={{color : 'blue'}}> Songs List </h3>
                    <div  style={{textAlign:"center"}}>
                    <table border = "1" style={{color : 'blue', width:'40%', textAlign:'left'}}>   
                    <thead>                     
                        <tr style={{width: '50%'}}>                          
                            <th >Movie</th>
                            <th>Title</th>
                            <th>Song Length</th>
                            <th>Singer</th> 
                        </tr>
                        </thead>
                       
                       
                        {this.state.posts.map((p) => (    
                             <tbody>  
                            <tr key= {p.id}>
                                <td>{p.movie}</td>
                                <td>{p.title}</td>
                                <td>{p.length}</td>
                                <td>{p.singer}</td>
                            </tr>  
                            </tbody>
                        )
                        )}                       
                    </table>
                    </div>
                    <br/>
                    <div>
                        <Link to="/addSong" ><button type="button" className="btn btn-info">Add Song</button></Link>
                    </div>                        
                </div>
            );
        }
}
export default GetSongsFromJSON;