import React from 'react'
import Header from "./Header";
import About from "./About";
import GetSongsFromJSON from "./GetSongsFromJSON";
import AddNewSong from './AddNewSong'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

class FormikImplementation extends React.Component {
 render(){

    return(
        <div>
            <Header />
            <Router>
                <Link to = "">About </Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Link to = "/songs"> Songs</Link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <Route exact path="/" component={About} />
                <Route  path="/songs" component={GetSongsFromJSON} />
                <Route  path="/addSong" component={AddNewSong} />
             </Router>       
        </div>
    );
 }   
}

export default FormikImplementation;